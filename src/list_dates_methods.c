#include "node_date.h"
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>

int add_date(Date to_add, Node_date * prev_date, Node_date * head){
    Node_date * new_node = malloc(sizeof(Node_date));
    if (new_node == NULL){
        printf("Ошибка с выделением памяти\n");
        return -1;
    }
    new_node->data = to_add;
    new_node->next = prev_date->next;
    prev_date->next = new_node;
    
    return OK;
}

Node_date* find_place_to_add_after(Date * given_date, Node_date *  head){
    if (head == NULL){
        printf("Ошибка в организации списка\n");
        return NULL;
    }
    if(head->next == NULL){
        return head;
    }
    Node_date * cur_node = head;
    while( (cur_node->next != NULL)){
        if ( which_is_more( &cur_node->next->data, given_date ) == SECOND ){
            cur_node = cur_node->next;
        } else {
            return cur_node;
        }
        
    }
    return cur_node;
}

size_t print_list(Node_date * head){ // возвращает количество напечатанных дат
    if (head == NULL){
        printf("Ошибка в организации списка\n");
        return ERR;
    }
    Node_date * cur_node = head;
    size_t count = 0;
    while (cur_node->next != NULL){
        cur_node = cur_node->next;
        print_date(&cur_node->data);
        count++;
    }
    return count;
}

int free_list(Node_date * head){
    if (head == NULL){
        printf("Ошибка в организации списка\n");
        return ERR;
    }
    Node_date * cur_node = NULL;
    while(head->next != NULL){
        cur_node = head;
        head = head->next;
        free(cur_node);
    }
    free(head);
    return OK;
}
 