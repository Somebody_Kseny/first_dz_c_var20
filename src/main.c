// Вариант #20
// Создать структуру для хранения информации о дате: номере года, номере месяца и числе.
// Составить с ее использованием программу вывода информации обо всех известных датах, начиная с наиболее ранней.

#include "date_methods.c"
#include "list_dates_methods.c"
#include <stdio.h>
#include <stddef.h>

int main (void)
{
  Node_date * head = malloc(sizeof(Node_date));
  if (head == NULL){
    printf("Ошибка с выделением памяти\n");
    return -1;
  }
  head->next = NULL;
  Date scanned_date;
  Date * s_date = &scanned_date;
  printf("Вводите даты в формате ДД.MM.ГГГГ\nПо окончании ввода введите 0\n");
  int input_state = INPUT_TRUE;
  do {
    input_state = scan_date(s_date);
    if(input_state == INPUT_TRUE){
      if(check_date(&scanned_date) == TRUE){
        Node_date * pl_to_add_after = find_place_to_add_after(s_date, head);
        if (pl_to_add_after == NULL){
          return -1;
        }
        if (add_date(scanned_date, pl_to_add_after, head) == ERR){
          printf("Ошибка!\n");
        }
      } else {
        printf("Не принято. Такой даты не существует.\n");
      }
    }
  } while(input_state != 1);
  printf("Результат:\n");
  if ( ( print_list(head) == ERR ) || ( free_list(head) == ERR ) ) {
    return -1;
  };
  return 0;
}