#include "date.h"
#include <stdio.h>

void print_date(const Date* got_date){
    printf("%02i.%02d.%d\n", got_date->day, got_date->month, got_date->year);
}

int which_is_more(const Date* date_1, const Date* date_2){ // возвращает 1 если первая больше или равна второй, иначе 2, ошибка -1 (наверно никогда)
    if (date_1->year > date_2->year){
        return FIRST;
    }
    if (date_1->year < date_2->year){
        return SECOND;
    } else { // ==
        if (date_1->month > date_2->month){
            return FIRST;
        }
        if (date_1->month < date_2->month){
            return SECOND;
        } else { // ==
            if (date_1->day >= date_2->day){
                return FIRST;
            }
            if (date_1->day < date_2->day){
                return SECOND;
            }
        }
    }
    return ERR;
}

int scan_date(Date * scanned_date){ // возвращает -1 при ошибке ввода, 0 при успехе, 1 при завершении пользователем
  int res_scan = scanf("%d.%d.%d", &scanned_date->day, &scanned_date->month, &scanned_date->year);
  while (getchar() != '\n');
  if((res_scan == 1) && (scanned_date->day == 0)){
    printf("Ввод закончен. Спасибо!\n");
    return INPUT_END;
  }
  if (res_scan != 3){
    printf("Не принято. Пожалуйста, дату в формате ДД.MM.ГГГГ или 0 для завершения\n");
    return INPUT_FALSE;
  }
  return INPUT_TRUE;
}

int check_date(const Date* got_date){ // 0 - верная, -1 - не существует
    int year = got_date->year;
    int month = got_date->month;
    int day = got_date->day;
    if((year >= 0) && ( (month >= 1) && (month <= 12) ) && ( (day >= 1) && (day <= 31))){
        if(month == FEB){
            if(year%4 == 0){
                if(day <= 29){
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                if(day <= 28){
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }
        if((month == JAN)||(month == MAR)||(month == MAY)||(month == JUL)||(month == AUG)||(month == OCT)||(month == DEC)){
            if(day <= 31){
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            if(day <= 30){
                return TRUE;
            } else {
                return FALSE;
            }
        }
    } else {
        return FALSE;
    }
    return ERR;
}

