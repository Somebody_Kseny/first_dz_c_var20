#include "gtest/gtest.h"
//#include "gmock/gmock.h"

extern "C" {
#include "include/date.h"
#include "include/node_date.h"
}

TEST(date_methods, which_is_more) {
  Date date_1 = {1, 12, 2010};
  Date date_2 = {2, 12, 2010};
  Date date_3 = {2, 11, 2010};
  Date date_4 = {2, 11, 2012};
  Date date_5 = {2, 7, 2019};
  Date date_6 = {3, 6, 2019};
  ASSERT_EQ(which_is_more( &date_1, &date_2 ), SECOND);
  ASSERT_EQ(which_is_more( &date_2, &date_1 ), FIRST);
  ASSERT_EQ(which_is_more( &date_2, &date_3 ), FIRST);
  ASSERT_EQ(which_is_more( &date_3, &date_4 ), SECOND);
  ASSERT_EQ(which_is_more( &date_4, &date_5 ), SECOND);
  ASSERT_EQ(which_is_more( &date_5, &date_6 ), FIRST);
  ASSERT_EQ(which_is_more( &date_6, &date_5 ), SECOND);
  ASSERT_EQ(which_is_more( &date_6, &date_1 ), FIRST);
}

TEST(date_methods, check_date) {
  Date date_1 = {52, 2, 2007};
  Date date_2 = {6, 42, 1990};
  Date date_3 = {1, 1, -80};
  Date date_4 = {0, 77, -80};
  Date date_5 = {29, 2, 0};
  Date date_6 = {7, 11, 1917};
  Date date_7 = {31, 2, 0};
  Date date_7_1 = {31, 2, 89};
  Date date_8 = {27, 2, 2003};
  Date date_9 = {9, 5, 2045};
  ASSERT_EQ(check_date( &date_1 ), FALSE);
  ASSERT_EQ(check_date( &date_2 ), FALSE);
  ASSERT_EQ(check_date( &date_3 ), FALSE);
  ASSERT_EQ(check_date( &date_4 ), FALSE);
  ASSERT_EQ(check_date( &date_5 ),  TRUE);
  ASSERT_EQ(check_date( &date_6 ),  TRUE);
  ASSERT_EQ(check_date( &date_7 ),  FALSE);
  ASSERT_EQ(check_date( &date_7_1 ),  FALSE);
  ASSERT_EQ(check_date( &date_8 ),  TRUE);
  ASSERT_EQ(check_date( &date_9 ),  TRUE);
}

TEST(list_dates_methods, first) {
  Node_date * head = (Node_date*)malloc(sizeof(Node_date));
  head->next = NULL;
  Date date_2 = {01, 01, 2000};
  ASSERT_EQ(find_place_to_add_after(&date_2, head), head);
  add_date(date_2, head, head); //ok
  ASSERT_EQ(print_list(head), 1);
  ASSERT_EQ(free_list(head), OK);
}

TEST(list_dates_methods, second) {
  Node_date * head = (Node_date*)malloc(sizeof(Node_date));
  head->next = NULL;
  Date date_1 = {01, 01, 2007};
  Date date_2 = {31, 12, 2006};
  Date date_3 = {01, 01, 2006};

  ASSERT_EQ(find_place_to_add_after(&date_2, head), head);
  add_date(date_2, head, head);
  ASSERT_EQ(print_list(head), 1);

  ASSERT_EQ(find_place_to_add_after(&date_1, head), head->next);
  add_date(date_1, head->next, head);
  ASSERT_EQ(print_list(head), 2);

  ASSERT_EQ(find_place_to_add_after(&date_3, head), head);
  add_date(date_3, head, head);
  ASSERT_EQ(print_list(head), 3);

  ASSERT_EQ(free_list(head), OK);
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
