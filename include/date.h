#ifndef DATE_H
#define DATE_H

enum return_value {
    TRUE,
    FALSE,

    FIRST,
    SECOND,

    INPUT_TRUE,
    INPUT_FALSE,
    INPUT_END,

    ERR,
    OK
};
enum months {
    JAN = 1,
    FEB = 2,
    MAR = 3,
    APR = 4,
    MAY = 5,
    JUN = 6,
    JUL = 7,
    AUG = 8,
    SPT = 9,
    OCT = 10,
    NVB = 11,
    DEC = 12
};

typedef struct Date {
    int day;
    int month;
    int year;
} Date;

void print_date(const Date*);
int which_is_more(const Date*, const Date*); // возвращает 1 если первая больше или равна второй, иначе 2
int scan_date(Date*);
int check_date(const Date*);

#endif //DATE_H