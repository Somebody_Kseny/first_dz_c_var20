#ifndef NODE_DATE_H
#define NODE_DATE_H

#include "date.h"
#include <stddef.h>

typedef struct Node_date Node_date;
struct Node_date {
    Date data;
    Node_date * next;
};

int add_date(Date, Node_date*, Node_date*);
Node_date* find_place_to_add_after(Date*, Node_date*);
size_t print_list(Node_date*);
int free_list(Node_date*);

#endif //NODE_DATE_H