###### Задание:
Вариант #20
Создать структуру для хранения информации о дате: номере года, номере месяца и числе. Составить с ее использованием программу вывода информации обо всех известных датах, начиная с наиболее ранней.
* Статический анализатор cppcheck
```
    Checking src/date_methods.c ...
    1/3 files checked 50% done
    Checking src/list_dates_methods.c ...
    2/3 files checked 76% done
    Checking src/main.c ...
    3/3 files checked 100% done
```
* Проверка valgrind
```
    ==30020== HEAP SUMMARY:
    ==30020==     in use at exit: 0 bytes in 0 blocks
    ==30020==   total heap usage: 233 allocs, 233 frees, 115,614 bytes allocated
    ==30020== 
    ==30020== All heap blocks were freed -- no leaks are possible
    ==30020== 
    ==30020== For lists of detected and suppressed errors, rerun with: -s
    ==30020== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```
* Оценка покрытия тестами gcovr
```
    ------------------------------------------------------------------------------
                            GCC Code Coverage Report
    Directory: ..
    ------------------------------------------------------------------------------
    File                                       Lines    Exec  Cover   Missing
    ------------------------------------------------------------------------------
    src/date_methods.c                            48      35    72%   29,32-37,39-41,43,70,76
    src/list_dates_methods.c                      43      35    81%   9-10,21-22,41-42,56-57
    src/main.c                                    23       0     0%   10,12-15,17,19-21,23-28,30-31,34,37-40,42
    tests.cpp                                     63      63   100%   
    ------------------------------------------------------------------------------
    TOTAL                                        177     133    75%
    ------------------------------------------------------------------------------
```
